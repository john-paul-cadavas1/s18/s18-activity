// console.log("Hello World")

/*
	Activity:
	1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

	2.) If health is less than or equal to 5, invoke faint function
*/

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	//methods
	this.tackle = function(target) {
		target.health = target.health - this.attack;
		if (target.health > 5){
			console.log(this.name + ' tackled '+ target.name);
			console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		} else {
			target.faint()
		}

	},
	this.faint = function() {
		console.log(this.name + " fainted.")
	}

}

let charizard = new Pokemon("Charizard", 40);
let pikachu = new Pokemon("Pikachu", 20);

pikachu.tackle(charizard);

pikachu.tackle(charizard);

pikachu.tackle(charizard);

pikachu.tackle(charizard);

pikachu.tackle(charizard);

pikachu.tackle(charizard);
